<?php


namespace App\Http\Controllers;



class PagesController extends Controller {

	public function getIndex() {
		//process variable data or params
		//talk to the model		
		//recieve from the model
		//compile or process data from model
		//pass data to the correct view
    		 
		
		return view('pages.welcome', ['title' => "Laravel Blog"]);

	}

	public function getAbout() {

		$first = "Luna";
		$second = "Albino Bill";
		$last = "Nala";

		$fullName = $first . ", " . $second . ", " .  "and  " . $last;
		$email = "princess@cute.com";
		$data = [];
		$data['email'] = $email;
		$data['fullname'] = $fullName;		
		return view('pages.about', ['title' => "About Me"])->withData($data);	

	}

	public function getContact() {

		return view('pages.contact', ['title' => "Contact Me"]);


	}	

}

?>
