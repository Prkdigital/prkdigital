@extends('main')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="jumbotron">
				<h1>Hello, Laravel!</h1>
				<p>Welcome to My Blog!</p>
				<p class="lead">Thank you for visiting. This is my test website build with laravel. Please read my popular post!</p>
				<p><a class="btn btn-primary btn-lg" href="#" role="button">Popular Post</a></p>
			</div>
		</div>
	</div><!-- end of header and row -->

	<div class=".row">
		<div class="col-md-8">
			<div class="post">
				<h3> $post->title </h3>
				<p class="lead"> $post->body </p>
				<a href="#" class="btn btn-primary">Read More</a>
			</div>

			<hr>

			<div class="post">
				<h3>Post Title</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sit amet commodo lorem, vitae varius libero. Nam vitae mi magna. Suspendisse viverra diam ut nibh tempor, non imperdiet purus cursus. Etiam vehicula sapien sit amet condimentum molestie.</p>
				<a href="#" class="btn btn-primary">Read More</a>
			</div>

			<hr>

			<div class="post">
				<h3>Post Title</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sit amet commodo lorem, vitae varius libero. Nam vitae mi magna. Suspendisse viverra diam ut nibh tempor, non imperdiet purus cursus. Etiam vehicula sapien sit amet condimentum molestie.</p>
				<a href="#" class="btn btn-primary">Read More</a>
			</div>

			<hr>

			<div class="post">
				<h3>Post Title</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sit amet commodo lorem, vitae varius libero. Nam vitae mi magna. Suspendisse viverra diam ut nibh tempor, non imperdiet purus cursus. Etiam vehicula sapien sit amet condimentum molestie.</p>
				<a href="#" class="btn btn-primary">Read More</a>
			</div>
		</div>
		<div class="col-md-3 col-md-offset-1">
			<h2>Sidebar</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ullamcorper neque lectus, vitae sodales nulla egestas ac. Ut convallis nisl tincidunt arcu vulputate scelerisque. Quisque ac fermentum eros, nec tristique neque. Nullam pretium maximus tempus. Duis et turpis eget odio pulvinar faucibus ac a elit. Orci varius natoque penatibus et magnis dis part</p>
		</div>			
	</div>
</div><!-- end of .container -->
@stop



<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
