@extends('main')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1>Contact Me</h1>
			<hr>
			<form>
				<div class="form-group">
					<label name="email">Email:</label>
					<input id="email" name="email" class="form-control">
				</div>

				<div class="form-group">
					<label name="subject">Subject:</label>
					<input id="subject" name="subject" class="form-control">
				</div>

				<div class="form-group">
					<label name="email">Email:</label>
					<textarea id="message" name="message" class="form-control">Type your message here...</textarea>
				</div>

				<input type="submit" value="Send Message" class="btn btn-success">
			</form>
		</div>
	</div><!-- end of header and row -->
</div><!-- end of .container -->


@stop












<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>